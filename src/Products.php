<?php 
namespace src;

use src\Connection;  
use src\Parse; 
class Products extends Connection {

    // public  $dbConnect = null; 

    function __constructor() {
        parent::__constructor(); 
        $parse = new Parse(); 

 
        $keyarrProducts = ["id", "product_name", "provider_name", "description", "weblink", "views","price"];
        forEach($parse->arrProduct as $arr) {
            $tabProduct = array_combine($keyarrProducts,$arr) ; 
            
            // echo "<pre>";
            // print_r($tabProduct);
            // echo "</pre>";
        
            $prepareStatement = $this->pdo->prepare("INSERT INTO products (product_name, provider_name, description, weblink, views, price) VALUES (:product_name, :provider_name, :description, :weblink, :views, :price)");
         
            $prepareStatement->bindParam(":product_name", $tabProduct["product_name"]);
            $prepareStatement->bindParam(":provider_name", $tabProduct["provider_name"]);
            $prepareStatement->bindParam(":description", $tabProduct["description"]);
            $prepareStatement->bindParam(":weblink", $tabProduct["weblink"]);
            $prepareStatement->bindParam(":views", $tabProduct["views"]);
            $prepareStatement->bindParam(":price", $tabProduct["price"]);
        
            $prepareStatement->execute(); 
            $prepareStatement->closeCursor();      
        
        
        }
        

    }

    function getAllProducts() {
        // $this->dbConnect = Connection::getConnection(); 
        $prepareStatement = $this->pdo->prepare("SELECT * FROM products"); 
        $prepareStatement->execute(); 
        $products = $prepareStatement->fetchAll();
        $prepareStatement->closeCursor(); 
        return $products;
    }


   
}