<?php 

namespace src; 

use src\Connection; 
use src\Parse; 

class Customer extends Connection {

    // connect to db... 
    //public  $dbConnect;
    // execute a prepare statement to insert 

    function __constructor() 
    {
        parent::__constructor();
        // $this->$dbConnect = Connection::getConnection();
        $parse = new Parse();
        $keyarr = ["id", "name", "lastName", "email", "phone","town","address","country"];

        foreach ($parse->arrCustomer as $arr) {
            $tabCustomer = array_combine($keyarr,$arr) ; 
            $prepareStatement = $this->pdo->prepare("INSERT INTO customers (id, name, lastName, email, phone) VALUES (:id, :name, :lastName, :email, :phone)");
            $prepareStatement->bindParam(":id", $tabCustomer["id"]);
            $prepareStatement->bindParam(":name", $tabCustomer["name"]);
            $prepareStatement->bindParam(":lastName", $tabCustomer["lastName"]);
            $prepareStatement->bindParam(":email", $tabCustomer["email"]);
            $prepareStatement->bindParam(":phone", $tabCustomer["phone"]);
            $prepareStatement->execute();
            $prepareStatement->closeCursor();
        }


        // print_r($parse->arrCustomer);

        // foreach ($parse->arrCustomer as $arr) {
        //     $tabCustomer = array_combine($keyarr,$arr) ; 
        //     // echo $tabCustomer["country"];
        //     echo $tabCustomer["address"];
        //     // echo $tabCustomer["name"]; 
        //     // echo $tabCustomer["lastName"]; 
        //     // echo $tabCustomer["email"]; 
            
        // }
           

    }

    function getAllCustomers() {
        // query SELECT 
        // $this->$dbConnect = Connection::getConnection();
        $sql = "SELECT * FROM customers"; 
        $prepareStatement  = $this->pdo->prepare($sql);
        $prepareStatement->execute();
        $customers = $prepareStatement->fetchAll();
        $prepareStatement->closeCursor();
        return $customers;
    }

}