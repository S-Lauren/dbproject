

CREATE TABLE customers(id INT PRIMARY KEY NOT NULL,name VARCHAR(100),lastName VARCHAR(100), email VARCHAR(255),phone VARCHAR(15));

CREATE TABLE providers
(
    id INT PRIMARY KEY NOT NULL,
    provider_name VARCHAR(100)
   
);

CREATE TABLE colors
(
    id INT PRIMARY KEY NOT NULL,
    color_name VARCHAR(100)
   
);

CREATE TABLE orders (
    id INT PRIMARY KEY NOT NULL,
    quantity INT,
    date_order DATETIME
);


CREATE TABLE products ( 
    id INT PRIMARY KEY AUTOINCREMENT NOT NULL,
    product_name VARCHAR(100),
    description VARCHAR(100),
    weblink VARCHAR(255),
    view VARCHAR(255),
    price REAL,
    id_provider INT,
    id_order INT,
    id_color INT,
    FOREIGN KEY(id_provider) REFERENCES providers(id),
    FOREIGN KEY(id_order) REFERENCES orders(id),
    FOREIGN KEY(id_color) REFERENCES colors(id)
   
);

CREATE TABLE adress
(
    id INT PRIMARY KEY NOT NULL,
    city VARCHAR(100),
    postalCode VARCHAR(100),
    country VARCHAR(255),
    id_customer INT,
    FOREIGN KEY(id_customer) REFERENCES customers(id)
   
);

CREATE TABLE sell
(
    id INT PRIMARY KEY NOT NULL,
    id_customer INT,
    id_product INT,
    FOREIGN KEY(id_customer) REFERENCES customers(id),
    FOREIGN KEY(id_product) REFERENCES products(id)
   
);