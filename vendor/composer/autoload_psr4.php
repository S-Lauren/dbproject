<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'src\\' => array($baseDir . '/src'),
    'Aspera\\Spreadsheet\\XLSX\\' => array($vendorDir . '/aspera/xlsx-reader/lib'),
);
